---
layout: Post
title: Catatan Pertama
subtitle: Halaman Belajar Markdown Style
author: khakimsetia
date: 2022-06-08
useHeaderImage: true
headerImage: /img/in-post/2022-06-08/header.jpg
headerMask: rgba(40, 57, 101, .4)
tags:
  - hello world
---

==Welcome== :smile: Senang sekali mendapat ilmu baru Belajar membuat Website Statis.

<!-- more -->

## Paragraphs
untuk `## H2 Heading` `### H3 Heading` `#### H4 Heading` `##### H5 Heading` `###### H6 Heading`

### Format Markdown

ini adalah contoh link [home page](/ "home page") by `[title](http://urlnya "Description is optional")`  
untuk `<br>` tulis spasi 2x di akhir tulisan  
untuk menulis teks **bold** `**bold**`, *italic* `*italic*` ***italic bold*** `***bold italic***`, dan ~~dicoret~~ `~~dicoret~~`  

> untuk blockquote `gunakan >`
>> menjorok
> (kosong)
> - tambah simbol  

- Untuk memulis List
- ini adalah list
    - test menjorok
    > tambah blockquote
- dan ini akhir

1. List Nomor
2. Nomor 2
    1. Sub nomornya
3. Ini terakhir

Menulis Emogi Icon
:smile: `:smile:`:smirk: `:smirk:`

### Format Image
Foto code : `![title](url-amge.png "Alt is optional")`
![logo](/img/logo/android-chrome-144x144.png "logo khakimsetia") 

## Code

### Inline Code

`var a = 1`
Penulisan didalam ` `` `

### Code Blocks
<code>
```languege{number yg ditandai - optional}
//isi kodingan
```</code>

Klik bulatan hijau untuk memperbesar code

```javascript{14}
const express = require('express');
const http = require('http');

const app = express();
const server = http.createServer(app);

//function response
function response(res, json){
	res.setHeader('Content-Type', 'application/json');
	res.end(JSON.stringify(json));
}

server.listen(config.app.port, () => {
    console.log('DITANDAI Server Ready => listening at http://localhost:${config.app.port}')
});
```


### Code Groups
<CodeGroup>

<CodeGroupItem title="NPM" active>

```bash
npm install -D express
```

</CodeGroupItem>

<CodeGroupItem title="YARN">

```bash
yarn add -D express
```

</CodeGroupItem>

</CodeGroup>

## Table

| Name | Info |
|------|------|
| contoh tabel | isi tabel |

## Badges
Penulisan `<Badge text="warning" type="warning" />`  
<Badge text="tip" /> <Badge text="warning" type="warning" /> <Badge text="danger" type="danger" /> <Badge text="tip middle" vertical="middle" />

## Charts

### Chart.js

```chart
{
  "type": "doughnut",
  "data": {
    "datasets": [{
      "data": [10, 20, 30],
      "backgroundColor": [
        "rgba(255, 99, 132)",
        "rgba(255, 206, 86)",
        "rgba(54, 162, 235)"
      ]
    }],
    "labels": ["Red", "Yellow", "Blue"]
  }
}
```

### Mermaid

```mermaidjs
flowchart
  A[Start] -->|Komputer tidak menyala| B(Periksa RAM)
  B -->|Nyalakan komputer| C{Kondisi}
  C -->|Nyala| D[Komputer Berjalan]
  C -->|Tidak| B
```

## Maths

Inline math: $E = mc^2$

Display math:

$$
i\hbar\frac{\partial \psi}{\partial t} = \frac{-\hbar^2}{2m} ( \frac{\partial^2}{\partial x^2} + \frac{\partial^2}{\partial y^2} + \frac{\partial^2}{\partial z^2} ) \psi + V \psi.
$$

With tags:

$$
\begin{gather}
  A = \text{softmax}(\frac{QK^T}{\sqrt{d_k}}) \\
  F_{\text{out}} = A V
\end{gather}
$$

## Containers
Format penulisan
```
::: info
this is info 
:::
```

::: link {/img/links/me.png} [My Blog](/)
Deskripsi My Blog.
:::

::: info
This is an info message.
:::

::: tip
This is a tip message.
:::

::: warning
This is a warning message.
:::

::: danger
This is a dangerous warning message.
:::

::: details Show me the code.
```cpp
cout << "Hello World!" << "\n";
```
:::

::: details Show me the code group.
<CodeGroup>
<CodeGroupItem title="JS" active>

```js
console.log("Hello World!");
```

</CodeGroupItem>

<CodeGroupItem title="PY">

```python
print("Hello World!")
```

</CodeGroupItem>
</CodeGroup>
:::