import { viteBundler } from "@vuepress/bundler-vite";
import { webpackBundler } from "@vuepress/bundler-webpack";
import { defineUserConfig } from "vuepress";
import { gungnirTheme, i18n } from "vuepress-theme-gungnir";
import { navbar, sidebar } from "./configs";

const isProd = process.env.NODE_ENV === "production";

export default defineUserConfig({
  base: "/",
  dest: "../public",
  head: [
    [
      "link",
      {
        rel: "icon",
        type: "image/png",
        sizes: "16x16",
        href: `/img/logo/favicon-16x16.png`
      }
    ],
    [
      "link",
      {
        rel: "icon",
        type: "image/png",
        sizes: "32x32",
        href: `/img/logo/favicon-32x32.png`
      }
    ],
    ["link", { rel: "manifest", href: "/manifest.webmanifest" }],
    ["meta", { name: "application-name", content: "Khakimsetia" }],
    ["meta", { name: "apple-mobile-web-app-title", content: "Catatan kecil Khakimsetia" }],
    [
      "meta",
      { name: "apple-mobile-web-app-status-bar-style", content: "black" }
    ],
    [
      "link",
      { rel: "apple-touch-icon", href: `/img/logo/apple-touch-icon.png` }
    ],
    ["meta", { name: "theme-color", content: "#377bb5" }],
    ["meta", { name: "msapplication-TileColor", content: "#377bb5" }]
  ],

  // site-level locales config
  locales: {
    "/": {
      lang: "en-US",
      title: "Music illution of dreaming kingdom - Khakimsetia",
      description: "Personal website yang berisi catatan kecil khakimsetia"
    }
  },

  // specify bundler via environment variable
  bundler:
    process.env.DOCS_BUNDLER === "webpack" ? webpackBundler() : viteBundler(),

  // configure default theme
  theme: gungnirTheme({
    repo: "khakimsetia/blog",
    docsDir: "docs",
    colorMode: 'dark',
    hitokoto: false,
    editLink: false,
    
    // personal information
    personalInfo: {
      name: "Khakimsetia",
      avatar: "/img/avatar.png",
      description: "Catatan kecil Khakimsetia",
      sns: {
        github: "Khakimsetia",
        telegram: {
          icon: "fa-telegram",
          link: "https://t.me/khakimsetia"
        },
        facebook: "khakim.setia",
        twitter: "khakimsetia",
        youtube: {
          icon: "fa-youtube",
          link: "https://youtube.com/khakimsetia"
        },
        email: "khakimsetia@gmail.com",
        rss: "/rss.xml"
      }
    },

    // header images on home page
    homeHeaderImages: [
      {
        path: "/img/home-bg/1.jpg",
        mask: "rgba(40, 57, 101, .4)"
      },
      {
        path: "/img/home-bg/2.jpg",
        mask: "rgb(251, 170, 152, .2)"
      },
      {
        path: "/img/home-bg/3.jpg",
        mask: "rgba(68, 74, 83, .1)"
      },
      {
        path: "/img/home-bg/4.jpg",
        mask: "rgba(19, 75, 50, .2)"
      }
    ],

    // other pages
    pages: {
      tags: {
        subtitle: "Tidak ada yang mustahil selama kita masih berusaha",
        bgImage: {
          path: "/img/pages/tags.jpg",
          mask: "rgba(211, 136, 37, .5)"
        }
      },
      links: {
        subtitle:
          "When you are looking at the stars, please put the brightest star shining night sky as my soul.",
        bgImage: {
          path: "/img/pages/links.jpg",
          mask: "rgba(64, 118, 190, 0.5)"
        }
      }
    },

    // theme-level locales config
    locales: {
      "/": {
        // navbar
        navbar: navbar.en,
        // sidebar
        sidebar: sidebar.en
      }
    },

    themePlugins: {
      // only enable git plugin in production mode
      git: isProd,
      katex: true,
      mermaid: true,
      chartjs: true,
      giscus: {
        repo: "khakimsetia/komentar",
        repoId: "R_kgDOHgS6Qw",
        category: "Announcements",
        categoryId: "DIC_kwDOHgS6Q84CPrXj",
        lazyLoad: true
      },
      mdPlus: {
        all: true
      },
      ga: "UA-232233174-1",
      rss: {
        siteURL: "https://khakimsetia.my.id",
        copyright: "Khakimsetia 2022"
      },
      pwa: true
    },

    footer: `
      &copy; <a href="https://github.com/khakimsetia/" target="_blank">Khakimsetia</a> 2022
    `
  }),

  markdown: {
    extractHeaders: {
      level: [2, 3, 4, 5]
    }
  }

});
